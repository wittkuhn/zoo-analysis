dt_behavior <- prepare_task_data(paths) %>%
  .[!(id %in% cfg$sub_exclude), ]
set.seed(666)
num_sub_select = 3
select_id = sample(x = unique(dt_behavior$id), size = num_sub_select)

dt_task <- dt_behavior %>%
  .[condition == "main", ] %>%
  .[, by = .(id, run, trial_run), ":="(
    long_interval = any(event_type == "iti" & duration == 10)
  )] %>%
  .[event_type == "response", ] %>%
  .[id %in% select_id, ] %>%
  .[long_interval == TRUE, ] %>%
  .[run == "run-01", ] %>%
  .[onset >= 100 & onset <= 300, ]

tr = 1.25
dt_sine_fits = load_data(file.path(paths$sourcedata, "zoo_sourcedata_decoding_recall_interval_sine_fit.csv"))
dt_task_fits = dt_task %>%
  merge.data.table(x = ., y = dt_sine_fits, by = c("id", "node")) %>%
  .[, params := lapply(transpose(.SD), c), .SDcols = c("frequency", "amplitude", "shift", "baseline")] %>%
  .[, by = .(id, onset, mask_test, run, trial_run, node), .(
    csine = sine_truncated(params = unlist(params), seq(0, 7, 1))
  )] %>%
  .[, by = .(id, onset, mask_test, run, trial_run, node), ":="(
    onset = onset + seq(0, 7, 1) * tr
  )] 

ggplot(data = dt_task) +
  geom_rect(aes(xmin = onset, xmax = onset + 10, ymin = 0, ymax = 1), fill = "lightgray", alpha = 0.3) +
  geom_segment(aes(x = onset, xend = onset, y = 0, yend = 0.3, color = node), size = 1) +
  geom_line(data = dt_task_fits, aes(x = onset, y = csine, group = interaction(node, trial_run), color = node)) +
  geom_point(data = dt_task_fits, aes(x = onset, y = csine, group = interaction(node, trial_run), color = node)) +
  facet_grid(vars(id), vars(run)) +
  coord_capped_cart(left = "both", bottom = "both") +
  xlab("Time from run onset (in seconds)") +
  ylab("Probability") +
  theme(axis.line = element_line(colour = "black")) +
  theme(panel.grid.major = element_blank()) +
  theme(panel.grid.minor = element_blank()) +
  theme(panel.border = element_blank()) +
  theme(panel.background = element_blank()) +
  theme(legend.position = "bottom") +
  theme(legend.direction = "horizontal") +
  theme(legend.justification = "center") +
  theme(legend.margin = margin(0, 0 ,0, 0)) +
  guides(colour = guide_legend(nrow = 1, title = "Stimulus")) +
  scale_color_viridis_d(option = "viridis")

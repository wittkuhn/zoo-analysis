# Zoo Analysis

[![pipeline status](https://git.mpib-berlin.mpg.de/wittkuhn/zoo-analysis/badges/master/pipeline.svg)](https://git.mpib-berlin.mpg.de/wittkuhn/zoo-analysis/-/commits/master) 

Project website: **https://wittkuhn.mpib.berlin/zoo-analysis/**

## Dataset structure

- All inputs (i.e. building blocks from other sources) are located in
  `inputs/`.
- All custom code is located in `code/`.

### Get input data

```bash
rclone config create zoo-bids seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library zoo-bids pass $CI_KEEPER_PASS
datalad siblings -d input/bids enable -s keeper
datalad siblings add -d input/bids -s gin --url git@gin.g-node.org:/lnnrtwttkhn/zoo-bids.git
datalad siblings configure -d input/bids -s origin --publish-depends keeper --publish-depends gin
datalad get --jobs 4 /bids/sub-*/*/func/*_events.tsv
```

## Downloads

- Download the **figures** of the latest build [here](https://git.mpib-berlin.mpg.de/wittkuhn/zoo-analysis/-/jobs/artifacts/master/browse/output/figures?job=pages)
- Download the **sourcedata files** of the latest build [here](https://git.mpib-berlin.mpg.de/wittkuhn/zoo-analysis/-/jobs/artifacts/master/browse/output/files?job=pages)

## Usage

### Install [DataLad](https://www.datalad.org/)

The analysis code uses `system2` commands to retrieve relevant data using [DataLad](https://www.datalad.org/).
Make sure that [DataLad](https://www.datalad.org/) is installed.
You can check by opening the Command Line / Terminal and type:

```bash
datalad --version
````

### Open `zoo-analysis.Rproj` from the Terminal

If you want to run the code in RStudio, in order for `datalad` commands to be available in the `system` command, it's recommended to open the R project file `zoo-analysis.Rproj` from the Command Line / Terminal, using:

```
open zoo-analysis.Rproj
```

### Run `code/utilities/setup.R` once

When you run the analyses interactively, you should open [`code/utilities/setup.R`](code/utilities/setup.R) and run it once, then enter all the `.Rmd`-files to continue with the analyses.



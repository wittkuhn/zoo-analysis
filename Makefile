all: index.html

index.html: index.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'index.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = yaml::read_yaml(here::here('parameters.yml'))\
	)"

zoo-analysis-questionnaire.html: code/questionnaire/zoo-analysis-questionnaire.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-questionnaire.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-behavior-hypotheses.html: code/behavior/zoo-analysis-behavior-hypotheses.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-behavior-hypotheses.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-behavior-training.html: code/behavior/zoo-analysis-behavior-training.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-behavior-training.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-behavior-main.html: code/behavior/zoo-analysis-behavior-main.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-behavior-main.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-behavior-main-sr.html: code/behavior/zoo-analysis-behavior-main-sr.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-behavior-main-sr.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-demographics.html: code/demographics/zoo-analysis-demographics.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-demographics.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-sessions.html: code/sessions/zoo-analysis-sessions.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-sessions.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"
	
zoo-analysis-decoding-recall.html: code/decoding/zoo-analysis-decoding-recall.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-decoding-recall.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"
	
zoo-analysis-decoding-main.html: code/decoding/zoo-analysis-decoding-main.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-decoding-main.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"
	
zoo-analysis-decoding-hypotheses.html: code/decoding/zoo-analysis-decoding-hypotheses.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-decoding-hypotheses.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

zoo-analysis-decoding-rest.html: code/decoding/zoo-analysis-decoding-rest.Rmd
	Rscript -e "rmarkdown::render(\
	input = '$<',\
	output_format = 'bookdown::html_document2',\
	output_file = 'zoo-analysis-decoding-rest.html',\
	output_dir = 'public',\
	output_yaml = here::here('_output.yml'),\
	clean = TRUE,\
	params = c(yaml::read_yaml(here::here('parameters.yml')), run_setup = TRUE)\
	)"

figures: code/figures/zoo-analysis-figures.R
	Rscript --vanilla '$<'

zoo_latest.sif:
	mkdir -p .singularity && \
	singularity pull --docker-login --force ".singularity/zoo_latest.sif" docker://registry.git.mpib-berlin.mpg.de/wittkuhn/zoo/bookdown:latest

slopes: code/decoding/zoo-analysis-decoding-main-slope.R
	Rscript --vanilla '$<'

slopes-tardis: code/decoding/zoo-analysis-decoding-slopes-tardis.R
	datalad unlock output/slopes && \
	Rscript --vanilla '$<'
